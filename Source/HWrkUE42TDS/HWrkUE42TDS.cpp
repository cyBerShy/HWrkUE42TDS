// Copyright Epic Games, Inc. All Rights Reserved.

#include "HWrkUE42TDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HWrkUE42TDS, "HWrkUE42TDS" );

DEFINE_LOG_CATEGORY(LogHWrkUE42TDS)
 