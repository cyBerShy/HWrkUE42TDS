// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class HWrkUE42TDS : ModuleRules
{
	public HWrkUE42TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
		PublicIncludePaths.AddRange(new string[]
        {
        "HWrkUE42TDS"
        });
    }
	
}
