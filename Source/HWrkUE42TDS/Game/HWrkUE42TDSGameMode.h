// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HWrkUE42TDSGameMode.generated.h"

UCLASS(minimalapi)
class AHWrkUE42TDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHWrkUE42TDSGameMode();
};



