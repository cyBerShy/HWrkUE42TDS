// Copyright Epic Games, Inc. All Rights Reserved.

#include "HWrkUE42TDSGameMode.h"
#include "HWrkUE42TDSPlayerController.h"
//#include "Character/HWrkUE42TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AHWrkUE42TDSGameMode::AHWrkUE42TDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHWrkUE42TDSPlayerController::StaticClass();

//	// set default pawn class to our Blueprinted character
//	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
//	if (PlayerPawnBPClass.Class != nullptr)
//	{
//		DefaultPawnClass = PlayerPawnBPClass.Class;
//	}
}